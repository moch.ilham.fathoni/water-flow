<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Histori;
use Illuminate\Http\Request;

class DataController extends Controller
{
    protected $urladafruitio = 'https://io.adafruit.com/api/v2/fathoni/feeds/';
    protected $iokey = 'aio_sxAA07eGlPMYAZodEK6t9zfhVYxN';
    
    public function getDataA() {
        $url = $this->urladafruitio . 'flow/data/last';

        $ch = curl_init(); // Initialize cURL
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('X-AIO-Key' => $this->iokey));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        curl_close($ch);
        $output = json_decode($response, true);

        return $output;
    }

    public function getDataB() {
        $url = $this->urladafruitio . 'flow2/data/last';

        $ch = curl_init(); // Initialize cURL
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('X-AIO-Key' => $this->iokey));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        curl_close($ch);
        $output = json_decode($response, true);

        return $output;
    }

    public function getDataC() {
        $url = $this->urladafruitio . 'flow3/data/last';

        $ch = curl_init(); // Initialize cURL
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('X-AIO-Key' => $this->iokey));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        curl_close($ch);
        $output = json_decode($response, true);

        return $output;
    }

    public function getDataD() {
        $url = $this->urladafruitio . 'flow4/data/last';

        $ch = curl_init(); // Initialize cURL
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('X-AIO-Key' => $this->iokey));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        curl_close($ch);
        $output = json_decode($response, true);

        return $output;
    }

    public function getDataE() {
        $url = $this->urladafruitio . 'flow5/data/last';

        $ch = curl_init(); // Initialize cURL
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('X-AIO-Key' => $this->iokey));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        curl_close($ch);
        $output = json_decode($response, true);

        return $output;
    }

    public function store(Request $request)
    {
        try {
            $histori = new Histori;
            $histori->value = $request->value;
            $histori->latitude = $request->lat;
            $histori->longitude = $request->lon;
            $histori->save();

            return response()->json(['success' => 'Data berhasil disimpan']);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        } catch (\Throwable $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function histori() {
        $data = Histori::orderBy('updated_at', 'desc')->limit(10)->get();
        return $data;
    }

}
