<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>

<body>
    <div class="py-5">
        <div class="container">
            <div class="row hidden-md-up">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <table id="tabel-status" class="table table-borderless" style="border: 1px solid black;">
                                <thead>
                                    <tr>
                                        <th colspan="3" class="text-center" style="border: 1px solid black;">Data Sensor
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>Device</th>
                                        <th>Status</th>
                                        <th>Value</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Sensor A:</td>
                                        <td>Disconnected</td>
                                        <td>0 L</td>
                                    </tr>
                                    <tr>
                                        <td>Sensor B:</td>
                                        <td>Disconnected</td>
                                        <td>0 L</td>
                                    </tr>
                                    <tr>
                                        <td>Sensor C:</td>
                                        <td>Disconnected</td>
                                        <td>0 L</td>
                                    </tr>
                                    <tr>
                                        <td>Sensor D:</td>
                                        <td>Disconnected</td>
                                        <td>0 L</td>
                                    </tr>
                                    <tr>
                                        <td>Sensor E:</td>
                                        <td>Disconnected</td>
                                        <td>0 L</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="row" style="grid-row-gap: 100px;">
                                <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-center">
                                    <div id="titik-a" class="titik">
                                        <div class="center-text-shape"><strong>A</strong></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6 d-flex justify-content-center">
                                    <div id="titik-b" class="titik">
                                        <div class="center-text-shape"><strong>B</strong></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6 d-flex justify-content-center">
                                    <div id="titik-c" class="titik">
                                        <div class="center-text-shape"><strong>C</strong></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6 d-flex justify-content-center">
                                    <div id="titik-d" class="titik">
                                        <div class="center-text-shape"><strong>D</strong></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6 d-flex justify-content-center">
                                    <div id="titik-e" class="titik">
                                        <div class="center-text-shape"><strong>E</strong></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <table id="tabel-histori" class="table table-borderless" style="border: 1px solid black;">
                                <thead>
                                    <tr>
                                        <th colspan="2" class="text-center" style="border: 1px solid black;">Histori
                                            Kebocoran</th>
                                    </tr>
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>Lokasi</th>
                                    </tr>
                                </thead>
                                <tbody style="font-size: 15px;">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            var terhubung = "Real time";
            var terputus = "Disconnected";
            var disconnColor = '#bbb';
            var connColor = '#27AE60';
            var warningColor = '#E74C3C';
            var timelimit = 60000;
            var bocor = 0.2; // Liter

            var setHistoriA = 0;
            var setHistoriB = 0;
            var setHistoriC = 0;
            var setHistoriD = 0;
            var setHistoriE = 0;

            function getHistori() {
                $.ajax({
                    url: "{{ url('histori') }}",
                    type: "GET",
                    data: {
                        format: 'json',
                        _token:"{{ csrf_token() }}"
                    },
                    sasdataType: 'json',
                    success: function(response){
                        var table = document.getElementById("tabel-histori").getElementsByTagName('tbody')[0];
                        while (table.childNodes.length) {
                            table.removeChild(table.childNodes[0]);
                        }
                        $.each(response, function(index,item){
                            var row = table.insertRow(index);
                            var cell1 = row.insertCell(0);
                            var cell2 = row.insertCell(1);
                            var tanggal = new Date(item.updated_at);
                            var dd = String(tanggal.getDate()).padStart(2, '0');
                            var mm = String(tanggal.getMonth() + 1).padStart(2, '0'); //January is 0!
                            var yyyy = tanggal.getFullYear();
                            cell1.innerHTML = dd + '-' + mm + '-' + yyyy;
                            cell2.innerHTML = "<a href='https://www.google.com/maps/place/" + item.latitude + " " + item.longitude + "' target='_blank'>" + item.latitude + ', ' + item.longitude + "</a>";
                        });
                        console.log(response);
                    }
                });
            }

            function simpanHistori(val, lat, lon) {
                $.ajax({
                    url: "{{ url('histori') }}",
                    type: "POST",
                    data: {
                        value:val,
                        lat:lat,
                        lon:lon,
                        _token:"{{ csrf_token() }}"
                    },
                    success: function(response){
                        console.log(response);
                    }
                });
            }

            function getA() {
                var conn = document.getElementById("tabel-status").rows[2].cells;
                var titikStat = document.getElementById("titik-a");
                $.ajax({
                    type: 'GET',
                    url: "{{ url('api/a') }}",
                    data: '_token = <?php echo csrf_token() ?>',
                    async: false,
                    success: function (response) {
                        if (response.error) {
                            conn[1].innerHTML = terputus;
                            conn[2].innerHTML = "0 L";
                            titikStat.style.backgroundColor = disconnColor;
                            console.log(response);
                        } else {
                            conn[2].innerHTML = response.value + " L";
                            var start = new Date(response.updated_at).getTime();
                            var end = new Date().getTime();
                            var diff = end - start;
                            if (diff > timelimit) {
                                conn[1].innerHTML = terputus;
                                titikStat.style.backgroundColor = disconnColor;
                            } else {
                                conn[1].innerHTML = terhubung;
                                if (response.value < bocor) {
                                    titikStat.style.backgroundColor = warningColor;
                                    if (setHistoriA == 0) {
                                        simpanHistori(response.value, response.lat, response.lon);
                                        setHistoriA = 1;
                                    }
                                } else {
                                    titikStat.style.backgroundColor = connColor;
                                    setHistoriA = 0;
                                }
                            }
                            console.log(response);
                        }
                    },
                    error: function (xhr, status, error) {
                        conn[1].innerHTML = terputus;
                        conn[2].innerHTML = "0 L";
                        titikStat.style.backgroundColor = disconnColor;
                        console.log(error);
                    }
                });
            }

            function getB() {
                var conn = document.getElementById("tabel-status").rows[3].cells;
                var titikStat = document.getElementById("titik-b");
                $.ajax({
                    type: 'GET',
                    url: "{{ url('api/b') }}",
                    data: '_token = <?php echo csrf_token() ?>',
                    async: false,
                    success: function (response) {
                        if (response.error) {
                            conn[1].innerHTML = terputus;
                            conn[2].innerHTML = "0 L";
                            titikStat.style.backgroundColor = disconnColor;
                            console.log(response);
                        } else {
                            conn[2].innerHTML = response.value + " L";
                            var start = new Date(response.updated_at).getTime();
                            var end = new Date().getTime();
                            var diff = end - start;
                            if (diff > timelimit) {
                                conn[1].innerHTML = terputus;
                                titikStat.style.backgroundColor = disconnColor;
                            } else {
                                conn[1].innerHTML = terhubung;
                                if (response.value < bocor) {
                                    titikStat.style.backgroundColor = warningColor;
                                    if (setHistoriB == 0) {
                                        simpanHistori(response.value, response.lat, response.lon);
                                        setHistoriB = 1;
                                    }
                                } else {
                                    titikStat.style.backgroundColor = connColor;
                                    setHistoriB = 0;
                                }
                            }
                            console.log(response);
                        }
                    },
                    error: function (xhr, status, error) {
                        conn[1].innerHTML = terputus;
                        conn[2].innerHTML = "0 L";
                        titikStat.style.backgroundColor = disconnColor;
                        console.log(error);
                    }
                });
            }

            function getC() {
                var conn = document.getElementById("tabel-status").rows[4].cells;
                var titikStat = document.getElementById("titik-c");
                $.ajax({
                    type: 'GET',
                    url: "{{ url('api/c') }}",
                    data: '_token = <?php echo csrf_token() ?>',
                    async: false,
                    success: function (response) {
                        if (response.error) {
                            conn[1].innerHTML = terputus;
                            conn[2].innerHTML = "0 L";
                            titikStat.style.backgroundColor = disconnColor;
                            console.log(response);
                        } else {
                            conn[2].innerHTML = response.value + " L";
                            var start = new Date(response.updated_at).getTime();
                            var end = new Date().getTime();
                            var diff = end - start;
                            if (diff > timelimit) {
                                conn[1].innerHTML = terputus;
                                titikStat.style.backgroundColor = disconnColor;
                            } else {
                                conn[1].innerHTML = terhubung;
                                if (response.value < bocor) {
                                    titikStat.style.backgroundColor = warningColor;
                                    if (setHistoriC == 0) {
                                        simpanHistori(response.value, response.lat, response.lon);
                                        setHistoriC = 1;
                                    }
                                } else {
                                    titikStat.style.backgroundColor = connColor;
                                    setHistoriC = 0;
                                }
                            }
                            console.log(response);
                        }
                    },
                    error: function (xhr, status, error) {
                        conn[1].innerHTML = terputus;
                        conn[2].innerHTML = "0 L";
                        titikStat.style.backgroundColor = disconnColor;
                        console.log(error);
                    }
                });
            }

            function getD() {
                var conn = document.getElementById("tabel-status").rows[5].cells;
                var titikStat = document.getElementById("titik-d");
                $.ajax({
                    type: 'GET',
                    url: "{{ url('api/d') }}",
                    data: '_token = <?php echo csrf_token() ?>',
                    async: false,
                    success: function (response) {
                        if (response.error) {
                            conn[1].innerHTML = terputus;
                            conn[2].innerHTML = "0 L";
                            titikStat.style.backgroundColor = disconnColor;
                            console.log(response);
                        } else {
                            conn[2].innerHTML = response.value + " L";
                            var start = new Date(response.updated_at).getTime();
                            var end = new Date().getTime();
                            var diff = end - start;
                            if (diff > timelimit) {
                                conn[1].innerHTML = terputus;
                                titikStat.style.backgroundColor = disconnColor;
                            } else {
                                conn[1].innerHTML = terhubung;
                                if (response.value < bocor) {
                                    titikStat.style.backgroundColor = warningColor;
                                    if (setHistoriD == 0) {
                                        simpanHistori(response.value, response.lat, response.lon);
                                        setHistoriD = 1;
                                    }
                                } else {
                                    titikStat.style.backgroundColor = connColor;
                                    setHistoriD = 0;
                                }
                            }
                            console.log(response);
                        }
                    },
                    error: function (xhr, status, error) {
                        conn[1].innerHTML = terputus;
                        conn[2].innerHTML = "0 L";
                        titikStat.style.backgroundColor = disconnColor;
                        console.log(error);
                    }
                });
            }

            function getE() {
                var conn = document.getElementById("tabel-status").rows[6].cells;
                var titikStat = document.getElementById("titik-e");
                $.ajax({
                    type: 'GET',
                    url: "{{ url('api/e') }}",
                    data: '_token = <?php echo csrf_token() ?>',
                    async: false,
                    success: function (response) {
                        if (response.error) {
                            conn[1].innerHTML = terputus;
                            conn[2].innerHTML = "0 L";
                            titikStat.style.backgroundColor = disconnColor;
                            console.log(response);
                        } else {
                            conn[2].innerHTML = response.value + " L";
                            var start = new Date(response.updated_at).getTime();
                            var end = new Date().getTime();
                            var diff = end - start;
                            if (diff > timelimit) {
                                conn[1].innerHTML = terputus;
                                titikStat.style.backgroundColor = disconnColor;
                            } else {
                                conn[1].innerHTML = terhubung;
                                if (response.value < bocor) {
                                    titikStat.style.backgroundColor = warningColor;
                                    if (setHistoriE == 0) {
                                        simpanHistori(response.value, response.lat, response.lon);
                                        setHistoriE = 1;
                                    }
                                } else {
                                    titikStat.style.backgroundColor = connColor;
                                    setHistoriE = 0;
                                }
                            }
                            console.log(response);
                        }
                    },
                    error: function (xhr, status, error) {
                        conn[1].innerHTML = terputus;
                        conn[2].innerHTML = "0 L";
                        titikStat.style.backgroundColor = disconnColor;
                        console.log(error);
                    }
                });
            }

            setInterval(function () {
                getA();
                getB();
                getHistori();
            }, 5000); // update data di web setiap 5 detik
        });
    </script>
</body>

</html>
