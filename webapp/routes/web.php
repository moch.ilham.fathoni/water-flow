<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DataController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('/konten', function () {
    return view('konten');
});

Route::get('api/a', [DataController::class, 'getDataA']);
Route::get('api/b', [DataController::class, 'getDataB']);
Route::get('api/c', [DataController::class, 'getDataC']);
Route::get('api/d', [DataController::class, 'getDataD']);
Route::get('api/e', [DataController::class, 'getDataE']);
Route::post('histori', [DataController::class, 'store']);
Route::get('histori', [DataController::class, 'histori']);