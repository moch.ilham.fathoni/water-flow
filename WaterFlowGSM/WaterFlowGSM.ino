
// Modem:
#define TINY_GSM_MODEM_SIM5360

// Set serial for debug console (to the Serial Monitor, default speed 115200)
#define SerialMon Serial

// Set serial for AT commands (to the module)
// Use Hardware Serial on Mega, Leonardo, Micro
#define SerialAT Serial1

// See all AT commands, if wanted
// #define DUMP_AT_COMMANDS

// Define the serial console for debug prints, if needed
#define TINY_GSM_DEBUG SerialMon

// Range to attempt to autobaud
#define GSM_AUTOBAUD_MIN 9600
#define GSM_AUTOBAUD_MAX 115200

// Add a reception delay - may be needed for a fast processor at a slow baud rate
// #define TINY_GSM_YIELD() { delay(2); }

// Define how you're planning to connect to the internet
#define TINY_GSM_USE_GPRS true
#define TINY_GSM_USE_WIFI false

// set GSM PIN, if any
#define GSM_PIN ""

// Your GPRS credentials, if any
const char apn[] = "internet";
const char gprsUser[] = "wap";
const char gprsPass[] = "wap123";

// Your WiFi connection credentials, if applicable
const char wifiSSID[] = "PDJT01";
const char wifiPass[] = "rumahjati94";

// MQTT details
const char* broker = "io.adafruit.com";
/************************* Adafruit.io Setup *********************************/
#define AIO_SERVER      "io.adafruit.com"
#define AIO_SERVERPORT  1883                   // use 8883 for SSL
#define AIO_USERNAME    "fathoni"
#define AIO_KEY         "aio_sxAA07eGlPMYAZodEK6t9zfhVYxN"
const char* topicInit = "fathoni/feeds/flow";
String clientName = "mqtttonn";
#include <TinyGsmClient.h>
#include <PubSubClient.h>

// Just in case someone defined the wrong thing..
#if TINY_GSM_USE_GPRS && not defined TINY_GSM_MODEM_HAS_GPRS
#undef TINY_GSM_USE_GPRS
#undef TINY_GSM_USE_WIFI
#define TINY_GSM_USE_GPRS false
#define TINY_GSM_USE_WIFI true
#endif
#if TINY_GSM_USE_WIFI && not defined TINY_GSM_MODEM_HAS_WIFI
#undef TINY_GSM_USE_GPRS
#undef TINY_GSM_USE_WIFI
#define TINY_GSM_USE_GPRS true
#define TINY_GSM_USE_WIFI false
#endif

#ifdef DUMP_AT_COMMANDS
#include <StreamDebugger.h>
StreamDebugger debugger(SerialAT, SerialMon);
TinyGsm modem(debugger);
#else
TinyGsm modem(SerialAT);
#endif
TinyGsmClient client(modem);
PubSubClient mqtt(client);

#define LED_PIN 13
int ledStatus = LOW;

uint32_t lastReconnectAttempt = 0;

#include <TinyGPS++.h>
//long   lat,lon; // create variable for latitude and longitude object
float lat, lon;
TinyGPSPlus gps; // create gps object

unsigned long previousMillis;
int interval = 5000; // 5 second


byte statusLed    = 13;
byte sensorInterrupt = 0;  // 0 = digital pin 2
byte sensorPin       = 2;
// The hall-effect flow sensor outputs approximately 4.5 pulses per second per
// litre/minute of flow.
float calibrationFactor = 4.5;
volatile byte pulseCount;
float flowRate;
float flowLitres;
float totalLitres;

unsigned int flowMilliLitres;
unsigned long totalMilliLitres;
unsigned long oldTime;

boolean mqttConnect() {
  SerialMon.print("Connecting to ");
  SerialMon.print(broker);

  // Connect to MQTT Broker:
  boolean status = mqtt.connect("GsmClientName", AIO_USERNAME, AIO_KEY);

  if (status == false) {
    SerialMon.println(" fail");
    return false;
  }
  SerialMon.println(" success");
  return mqtt.connected();
}


void setup() {
  // Set console baud rate
  SerialMon.begin(115200);
  delay(10);

  pinMode(LED_PIN, OUTPUT);

  // !!!!!!!!!!!
  // Set your reset, enable, power pins here
  // !!!!!!!!!!!

  SerialMon.println("Wait...");

  // Set GSM module baud rate
  TinyGsmAutoBaud(SerialAT, GSM_AUTOBAUD_MIN, GSM_AUTOBAUD_MAX);
  // SerialAT.begin(9600);
  delay(6000);

  // Restart takes quite some time
  // To skip it, call init() instead of restart()
  SerialMon.println("Initializing modem...");
  modem.restart();
  // modem.init();

  String modemInfo = modem.getModemInfo();
  SerialMon.print("Modem Info: ");
  SerialMon.println(modemInfo);

#if TINY_GSM_USE_GPRS
  // Unlock your SIM card with a PIN if needed
  if ( GSM_PIN && modem.getSimStatus() != 3 ) {
    modem.simUnlock(GSM_PIN);
  }
#endif

#if TINY_GSM_USE_WIFI
  // Wifi connection parameters must be set before waiting for the network
  SerialMon.print(F("Setting SSID/password..."));
  if (!modem.networkConnect(wifiSSID, wifiPass)) {
    SerialMon.println(" fail");
    delay(10000);
    return;
  }
  SerialMon.println(" success");
#endif

#if TINY_GSM_USE_GPRS && defined TINY_GSM_MODEM_XBEE
  // The XBee must run the gprsConnect function BEFORE waiting for network!
  modem.gprsConnect(apn, gprsUser, gprsPass);
#endif

  SerialMon.print("Waiting for network...");
  if (!modem.waitForNetwork()) {
    SerialMon.println(" fail");
    delay(10000);
    return;
  }
  SerialMon.println(" success");

  if (modem.isNetworkConnected()) {
    SerialMon.println("Network connected");
  }

#if TINY_GSM_USE_GPRS
  // GPRS connection parameters are usually set after network registration
  SerialMon.print(F("Connecting to "));
  SerialMon.print(apn);
  if (!modem.gprsConnect(apn, gprsUser, gprsPass)) {
    SerialMon.println(" fail");
    delay(10000);
    return;
  }
  SerialMon.println(" success");

  if (modem.isGprsConnected()) {
    SerialMon.println("GPRS connected");
  }
#endif

  // MQTT Broker setup
  mqtt.setServer(broker, AIO_SERVERPORT);

  Serial.println("The GPS Received Signal:");
  Serial3.begin(9600); // connect gps sensor

  // Set up the status LED line as an output
  pinMode(statusLed, OUTPUT);
  digitalWrite(statusLed, HIGH);  // We have an active-low LED attached

  pinMode(sensorPin, INPUT);
  digitalWrite(sensorPin, HIGH);

  pulseCount        = 0;
  flowRate          = 0.0;
  flowMilliLitres   = 0;
  totalMilliLitres  = 0;
  oldTime           = 0;

  // The Hall-effect sensor is connected to pin 2 which uses interrupt 0.
  // Configured to trigger on a FALLING state change (transition from HIGH
  // state to LOW state)
  attachInterrupt(sensorInterrupt, pulseCounter, FALLING);
 
}

//=========================================================
void loop() {
  if((millis() - previousMillis) > interval) {
    int w = 0;
    while (Serial3.available() > 0)
     if (gps.encode(Serial3.read()))
        if (w < 1) {
          gpsInfo();
        }
        w++;
  }
}

/*
  Insterrupt Service Routine
*/
void pulseCounter()
{
  // Increment the pulse counter
  pulseCount++;
}

void callback(char* topic, byte* payload, unsigned int length) {
  // handle message arrived
  //  Serial.print("Message arrived [");
  //  Serial.print(topic);
  //  Serial.print("] ");
  //  for (int i = 0; i < length; i++) {
  //    Serial.print((char)payload[i]);
  //  }
  //  Serial.println();
  //
  //  // Switch on the LED if an 1 was received as first character
  //  if ((char)payload[0] == '1') {
  //    digitalWrite(BUILTIN_LED, LOW);   // Turn the LED on (Note that LOW is the voltage level
  //    // but actually the LED is on; this is because
  //    // it is active low on the ESP-01)
  //  } else {
  //    digitalWrite(BUILTIN_LED, HIGH);  // Turn the LED off by making the voltage HIGH
  //  }

}
void waterFlowInfo() {
  detachInterrupt(sensorInterrupt);
  
  flowRate = ((1000.0 / (millis() - previousMillis)) * pulseCount) / calibrationFactor;
  previousMillis = millis();
  flowLitres = (flowRate / 60);     
  totalLitres += flowLitres;
  unsigned int frac;
  Serial.print("flowrate: ");
  Serial.print(int(flowRate));  // Print the integer part of the variable
  Serial.print(".");             // Print the decimal point
  frac = (flowRate - int(flowRate)) * 10;
  Serial.print(frac, DEC) ;      // Print the fractional part of the variable
  Serial.print("L/min");
  Serial.print("  Current Liquid Flowing: ");             // Output separator
  Serial.print(flowLitres);
  Serial.print("L / 5 Sec");
  Serial.print("  Output Liquid Quantity: ");             // Output separator
  Serial.print(totalLitres);
  Serial.print("L");

  pulseCount = 0;
  attachInterrupt(sensorInterrupt, pulseCounter, FALLING);
}

void gpsInfo() {
  Serial.print(F("Location: ")); 
  if (gps.location.isValid())
  {
    Serial.print("Latitude=");
    Serial.print(gps.location.lat(), 6);
    Serial.print(F(", "));
    Serial.print("Longitude=");
    Serial.println(gps.location.lng(), 6);
    waterFlowInfo();
    pubData(String(flowLitres), String(gps.location.lat(), 6), String(gps.location.lng(), 6));
  }
  else
  {
    Serial.println(F("INVALID"));
  }
  Serial.println();
}

void pubData(String nilai, String latitude, String longitude) {
  String payload = "{\"value\":\"" + nilai;
  payload += "\",\"lat\":\"" + latitude;
  payload += "\",\"lon\":\"" + longitude;
  payload += "\"}";

if (!mqtt.connected()) {
        SerialMon.println("=== MQTT NOT CONNECTED ===");
        // Reconnect every 10 seconds
        uint32_t t = millis();
        if (t - lastReconnectAttempt > 10000L) {
          lastReconnectAttempt = t;
          if (mqttConnect()) {
            lastReconnectAttempt = 0;
          }
        }
        delay(100);
        return;
      }

      mqtt.loop();
      if (mqtt.publish(topicInit,(char*) payload.c_str())) {
      Serial.println("Publish ok");
  } else {
      Serial.println("Publish failed");
  }
}
