#include <ESP8266WiFi.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"
#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#define PULSE_PIN D2  //gpio4

unsigned long previousMillis;
int interval = 5000; // 5 second

volatile long pulseCount = 0;
float calibrationFactor = 4.5;
float flowRate;
float flowLitres;
float totalLitres;
uint32_t x = 0;

static const int TXPin = 0, RXPin = 5;
static const uint32_t GPSBaud = 9600;
TinyGPSPlus gps;

// The serial connection to the GPS device
SoftwareSerial ss(TXPin, RXPin);

/************************* WiFi Access Point *********************************/
#define WLAN_SSID       "PDJT01"
#define WLAN_PASS       "rumahjati94"

/************************* Adafruit.io Setup *********************************/
#define AIO_SERVER      "io.adafruit.com"
#define AIO_SERVERPORT  1883                   // use 8883 for SSL
#define AIO_USERNAME    "fathoni"
#define AIO_KEY         "aio_sxAA07eGlPMYAZodEK6t9zfhVYxN"

/************ Global State (you don't need to change this!) ******************/
// Create an ESP8266 WiFiClient class to connect to the MQTT server.
WiFiClient client;
// or... use WiFiFlientSecure for SSL
//WiFiClientSecure client;

// Setup the MQTT client class by passing in the WiFi client and MQTT server and login details.
Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, AIO_USERNAME, AIO_KEY);

/****************************** Feeds ***************************************/
// Setup a feed called 'flow' for publishing.
// Notice MQTT paths for AIO follow the form: <username>/feeds/<feedname>
Adafruit_MQTT_Publish flow = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/flow");

/*************************** Sketch Code ************************************/
// Bug workaround for Arduino 1.6.6, it seems to need a function declaration
// for some reason (only affects ESP8266, likely an arduino-builder bug).

void MQTT_connect();

void ICACHE_RAM_ATTR pulseCounter()
{
  pulseCount++;
}

void setup() {
  Serial.begin(115200);
  delay(10);

  // define value
  pulseCount      = 0;
  flowRate        = 0.0;
  flowLitres      = 0.0;
  previousMillis  = 0; 

  ss.begin(GPSBaud);
  pinMode(PULSE_PIN, INPUT);
  attachInterrupt(PULSE_PIN, pulseCounter, FALLING);

  // Connect to WiFi access point.
  Serial.print("Connecting to ");
  Serial.println(WLAN_SSID);

  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();

  Serial.println("WiFi connected");
  Serial.println("IP address: "); 
  Serial.println(WiFi.localIP());
  Serial.println();
}

void loop() {
  if((millis() - previousMillis) > interval) {
    // Ensure the connection to the MQTT server is alive (this will make the first
    // connection and automatically reconnect when disconnected).  See the MQTT_connect
    // function definition further below.
    MQTT_connect();
    
    int w = 0;
    while (ss.available() > 0)
     if (gps.encode(ss.read()))
        if (w < 1) {
          gpsInfo();
        }
        w++;

    if (millis() > 5000 && gps.charsProcessed() < 10)
    {
      Serial.println(F("No GPS detected: check wiring."));
      while(true);
    }
  
    // ping the server to keep the mqtt connection alive
    // NOT required if you are publishing once every KEEPALIVE seconds
    
//    if(! mqtt.ping()) {
//      mqtt.disconnect();
//    }
 }
}

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println("MQTT Connected!");
}

void waterFlowInfo() {
  detachInterrupt(PULSE_PIN);
  
  flowRate = ((1000.0 / (millis() - previousMillis)) * pulseCount) / calibrationFactor;
  previousMillis = millis();
  flowLitres = (flowRate / 60);     
  totalLitres += flowLitres;
  unsigned int frac;
  Serial.print("flowrate: ");
  Serial.print(int(flowRate));  // Print the integer part of the variable
  Serial.print(".");             // Print the decimal point
  frac = (flowRate - int(flowRate)) * 10;
  Serial.print(frac, DEC) ;      // Print the fractional part of the variable
  Serial.print("L/min");
  Serial.print("  Current Liquid Flowing: ");             // Output separator
  Serial.print(flowLitres);
  Serial.print("L / 5 Sec");
  Serial.print("  Output Liquid Quantity: ");             // Output separator
  Serial.print(totalLitres);
  Serial.print("L");

  pulseCount = 0;
  attachInterrupt(PULSE_PIN, pulseCounter, FALLING);
}

void gpsInfo() {
  Serial.print(F("Location: ")); 
  if (gps.location.isValid())
  {
    Serial.print("Latitude=");
    Serial.print(gps.location.lat(), 6);
    Serial.print(F(", "));
    Serial.print("Longitude=");
    Serial.println(gps.location.lng(), 6);
    waterFlowInfo();
    pubData(String(flowLitres), String(gps.location.lat(), 6), String(gps.location.lng(), 6));
  }
  else
  {
    Serial.println(F("INVALID"));
  }
  Serial.println();
}

void pubData(String nilai, String latitude, String longitude) {
  String payload = "{\"value\":\"" + nilai;
  payload += "\",\"lat\":\"" + latitude;
  payload += "\",\"lon\":\"" + longitude;
  payload += "\"}";
  
  Serial.print("Sending payload: ");
  Serial.println(payload);

  if (flow.publish((char*) payload.c_str())) {
      Serial.println("Publish ok");
  } else {
      Serial.println("Publish failed");
  }
}
